import React, {PropTypes} from 'react';
import {Link, IndexLink} from 'react-router';
import Loader from 'react-dots-loader';
import 'react-dots-loader/index.css';

const Header = ({loading}) => {
		return (
			<div className="nav">
				<IndexLink to="/" activeClassName="active">Home</IndexLink>
				{" | "}
				<Link to="/courses" activeClassName="active">Courses</Link>
				{" | "}
				<Link to="/about" activeClassName="active">About</Link>
				{loading && <Loader size={8} distance={8}  style={{'marginLeft':'10px'}}/>}
			</div>
			);	
};

Header.propTypes = {
	loading: PropTypes.bool.isRequired
};

export default Header;