import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as courseActions from '../../actions/courseActions';
import CourseForm from './CourseForm';
import toastr from 'toastr';

class ManageCoursePage extends React.Component {
	constructor(props, context) {
		super(props, context);
		this.updateCourseState = this.updateCourseState.bind(this);
		this.saveCourse = this.saveCourse.bind(this);

		this.state = {
			course: Object.assign({}, this.props.course),
			errors: {},
			saving: false
		};

	}

	componentWillReceiveProps(nextProps) {
		if (this.state.course.id != nextProps.course.id) {
			this.setState({course: nextProps.course});
		}
	}

	updateCourseState(event) {
		const field = event.target.name;
		const course = Object.assign({}, this.state.course);
		course[field] = event.target.value;
		return this.setState({
			course
		});
	}

	saveCourse(event) {
		event.preventDefault();
		this.setState({
			saving: true
		});
		this.props.actions.saveCourse(this.state.course)
											.then(() => this.redirect())
											.catch(error => this.showError(error));
	}

	showError(error) {
		this.setState({
			saving: false
		});
		toastr.error(error);
	}

	redirect() {
		this.setState({
			saving: false
		});
		toastr.success('Course Saved');
		this.context.router.push('/courses');
	}

	render() {
		return (
				<CourseForm 
								allAuthors={this.props.authors}
								course={this.state.course}
								onChange={this.updateCourseState}
								onSave={this.saveCourse}
								saving={this.state.saving}
								errors={this.state.errors} />
			);
	}
}

ManageCoursePage.propTypes = {
	course: PropTypes.object.isRequired,
	authors: PropTypes.array.isRequired,
	actions: PropTypes.object.isRequired
};

ManageCoursePage.contextTypes = {
	router: PropTypes.object
};

function getCourseById(courseId, courses) {
	let course = courses.filter(course => course.id == courseId);

	if(course.length > 0) {
		return course[0];
	}
	return null;
}

function mapStateToProps(state, ownProps) {

	let course = {id: '', watchHref: '', title: '', authorId: '', length: '', category: '' };
	if(ownProps.params.id) {
		course = getCourseById(ownProps.params.id, state.courses) || course;
	}

	const formattedDataFromAuthors = state.authors.map(author => {
			return {
				value: author.id,
				text: author.firstName + " " + author.lastName
			};
		});

	return {
		course: course,
		authors: formattedDataFromAuthors
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(courseActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageCoursePage);