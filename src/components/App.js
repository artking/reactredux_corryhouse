import React, {PropTypes} from 'react';
import Header from './common/Header';
import {connect} from 'react-redux';

class App extends React.Component {
	constructor () {
		super();
	}

	render () {
		return (
			<div className="container-fluid">
				<Header loading = {this.props.loading} />
				{this.props.children}
			</div>
			);
	}
}

App.propTypes = {
	loading: PropTypes.bool.isRequired,
	children: PropTypes.node.isRequired
};

function mapStateToProps(state, ownProps) {
	return {
		loading: state.ajaxCallsInProgress > 0
	};
}

export default connect(mapStateToProps)(App);