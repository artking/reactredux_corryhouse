import * as types from "../actions/actionTypes";
import initialState from "./initialState";

function endsWithSuccess(type) {
	if (type.substring(type.length-8) === "_SUCCESS") {
		return true;
	}
	return false;
}

export default function (state = initialState.ajaxCallsInProgress, action) {
	if (action.type == types.BEGIN_AJAX_CALL) {
		return state + 1;
	} else if (action.type == types.AJAX_CALL_ERROR || endsWithSuccess(action.type)) {
		return state - 1;
	}
	return state;
}